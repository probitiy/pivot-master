module Pivot
  class Tracker
    def self.count(items)
      items.size
    end

    def self.item_for(items, assignee)
      items.select { |item| item[:assignee] == assignee }.last
    end

    def self.pivoted?(items, assignee)
      !!items.detect { |item| item[:assignee] == assignee }
    end

    def self.total_points(items, options = {})
      if options[:assignee]
        selected_items = items.select { |item| item[:assignee] == options[:assignee] }
      else
        assignees = items.map { |item| item[:assignee] }.uniq
        selected_items = assignees.map { |assignee| item_for(items, assignee) }
      end

      selected_items.sum { |item| item[:points] }
    end

    def self.unique_assignees(items)
      items.uniq { |item| item[:assignee] }
    end
  end
end