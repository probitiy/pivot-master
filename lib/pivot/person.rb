module Pivot
  class Person
    attr_reader :email, :first_name, :last_name, :items

    def initialize(opts)
      @email = opts[:email]
      @first_name = opts[:first_name]
      @last_name = opts[:last_name]
      @items = []
    end

    def add_item(item, &block)
      item.assignee = email
      items.push(item)
      block.call if block_given?
    end
  end
end