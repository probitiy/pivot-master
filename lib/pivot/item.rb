module Pivot
  class Item
    attr_reader :name, :points
    attr_accessor :assignee

    def initialize(opts)
      @name = opts[:name]
      @assignee = opts[:assignee]
      @points = opts[:points]
    end

    def +(another_item)
      points + another_item.points
    end

    def project_code
      name.split('-')[0]
    end

    def valid?
      ['EREC', 'AZR'].include?(project_code)
    end
  end
end